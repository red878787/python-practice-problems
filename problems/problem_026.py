# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    # list between 0-100
    average = sum(values) / len(values)

    if average >= 90:
        print("A")
    elif average >= 80 and average < 90:
        print("B")
    elif average >= 70 and average < 80:
        print("C")
    elif average >= 60 and average < 70:
        print("D")
    else:
        print("F")

test = calculate_grade([70, 90, 34, 67])
print(test)